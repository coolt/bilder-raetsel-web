# Name des Spiels

Laden spiele auf Webseite (wie Rätsel Claudia Web)

Was man braucht:

- Minimales HTML
- Git Directory
- Platz auf Server
- Server von Github updaten
- Bild mit Auflösung um die 800 x 600.


Wie es funktioniert:

- Jedes Rätsel ist eine HTML-Seite
- (Seiten können per Passwort geschützt werden)
- Seiten können in Verzeichnissen sein
- index.html wird automatisch angezeigt
- Hinweise im Titel, Bild und Text
- Links können im Bild versteckt sein (image map)


## Minimales HTML

```html
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <title>title</title>
    <style>
	</style>
  </head>
  <body>
    <img src="">
	<p>Text</p>
	<a href="">text oder img</a>
  </body>
</html>
```

## Image Map HTML

Wechselt bei click zu nächster Seite
```html
<map name="a">
	<area shape="circle" coords="200,250,25" href="another.html" />
	<area shape="default" />
</map>
<img src="Eingang.jpg" usemap="#a"> <!-- fitting incompatible with map -->
```

## Image Map with audio on click HTML

```html
<map name="b">
	<area shape="rect" coords="145,145, 170,170" onclick="document.getElementById('tong').play(); return false;" href="" />
	<area shape="default" />
</map>

<img src="Eingang.jpg" usemap="#b">

<audio
id="tong"
src="tong.mp3">
</audio>
```

https://developer.mozilla.org/de/docs/Web/HTML/Element/map

## Text oder Link in Body
```html
 <body>
	<p>Text</p>
	<a href="">text oder img</a>
 </body>
 ```
 
 ## Wenn nichts raus kommt (Fehler)
 
 F12 (developer tools) > Console > Neuladen
 
## Mit Webserver auf localhost

Git bash starten und: 

In der Bash in das Verzeichnis wechseln, in dem index.html liegt.
Der Befehl cd heisst "change directory" und danach kann man das Verzeichnis von Hand eingeben (zwischen zwei '  ') oder man zieht den Ordner in die Bash und der Pfad kommt von selbst.

Hier ein Bsp.:

```
cd '/c/Users/katri_000/Documents/Freizeit/Ferien/Bilder Raten/'
```

Nun Php ausführen. Man muss dazu die Datei php.exe finden. In diesem Beispiel
wurde Php im Laufwerk C unter Programme installiert. Als localhost wird die Adresse
8080 gewählt.
```
'/c/Program Files/php-8.0.1/php.exe' -S localhost:8080
```

Nun im Browser localhost:8080 eingeben und die Datei index.html wird angezeigt.


## Git

```
git status
git add index.html
git commit . -m "text"
git push
```